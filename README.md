# Mia's Chrome Scripts

Does a variety of things that Mia found useful.

## Installation

1. Download the repository
2. Open Chrome
3. Open the chrome extensions page
4. Turn on Developer Mode if it is not already on
5. Click "Load unpacked extension..."
6. Pick the folder where this repository is saved

## Features

### Office 365 - Outlook
- Move the links to calendar/mail/etc to the top of the page
- Move next appointment to the top of the page and make it larger

### Jira
- Move the quicklinks to the right and make them take less space
- Adds a "🙃" button to remove useless user story syntax

### Jenkins
- Change max width of code blocks for readability

### Browser Action
- Show Gitlab Merge Requests (must log in via options page)
- Colorize the current page

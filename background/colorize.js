

function colorize() {
    var src = `
        var allElements = document.querySelectorAll('body, body *');
        for (var i=0; i < allElements.length; i++) {
            allElements[i].style.background = '#'+(Math.floor(Math.random()*16777216)).toString(16);
            allElements[i].style.color = '#'+(Math.floor(Math.random()*16777216)).toString(16);
        }
    `;
    chrome.tabs.getSelected((tab) => {
        chrome.tabs.executeScript(tab.id, {code:src});
    });
}

export default colorize;


(function () {
    'use strict';

    var userStories = -1;
    function aughUserStorySyntax() {
        let summaries = document.querySelectorAll('.ghx-issues .js-issue .ghx-summary .ghx-inner, .ghx-swimlane-header .ghx-summary');
        for (let i in summaries) {
            if (!summaries[i] || !summaries[i].innerHTML) {
                continue;
            }
            summaries[i].innerHTML = summaries[i].innerHTML.replace(/As (the|an|a) .* I (would like|would|want)( a | an | to)?/i, '...').replace(/so that .*$/i, '...');
        }
        e.preventDefault();
        return false;
    }
    function highlightUserStorySyntax() {
        let summaries = document.querySelectorAll('.ghx-issues .js-issue .ghx-summary .ghx-inner, .ghx-swimlane-header .ghx-summary');
        for (let i in summaries) {
            if (!summaries[i] || !summaries[i].innerHTML) {
                continue;
            }
            summaries[i].innerHTML = summaries[i].innerHTML.replace(/(As (the|an|a) .* I (would like|would|want)( a | an | to )?)/i, '<span class="useless">$1</span>').replace(/(so that .*)$/i, '<span class="useless">$1</span>');
        }
    }
    function doUserStories(e) {
        if (userStories === -1) {
            highlightUserStorySyntax();
            userStories = 0;
        }
        userStories = (userStories + 1) % 3;
        switch (userStories) {
            case 0:
                document.body.classList.remove('highlight-useless');
                document.body.classList.remove('hide-useless');
                break;
            case 1:
                document.body.classList.add('highlight-useless');
                document.body.classList.remove('hide-useless');
                break;
            case 2:
                document.body.classList.remove('highlight-useless');
                document.body.classList.add('hide-useless');
                break;
        }
        e.preventDefault();
        return false;
    }
    function selectSprint(e) {
        let newSprint = document.location.search.match(/sprint=(\d+)/)[1];
        let sprint = document.getElementById('sprint');
        sprint.href = `/secure/RapidBoard.jspa?rapidView=${getCurrentBoard()}&sprint=${newSprint}`;
        window.localStorage.setItem('currentSprint', newSprint);
        e.preventDefault();
        return false;
    }
    function selectBoard(e) {
        let newBoard = document.location.search.match(/rapidView=(\d+)/)[1];
        let sprint = document.getElementById('sprint');
        let board = document.getElementById('board');
        sprint.href = `/secure/RapidBoard.jspa?rapidView=${newBoard}&sprint=${getCurrentSprint()}`;
        board.href = `/secure/RapidBoard.jspa?rapidView=${newBoard}&view=planning`;
        window.localStorage.setItem('currentBoard', newBoard);
        e.preventDefault();
        return false;
    }
    function getCurrentSprint() {
        return window.localStorage.getItem('currentSprint');
    }
    function getCurrentBoard() {
        return window.localStorage.getItem('currentBoard');
    }

    function parent(el, test) {
        if (!el || el.parentNode === el) {
            return null;
        }
        if (test(el)) {
            return el;
        }
        return parent(el.parentNode, test);
    }

    function updateMenu() {
        let nurr = document.createElement('span');
        nurr.id = 'nurr';
        nurr.classList.add('hotlink');
        nurr.innerHTML = '🙃';
        nurr.title = 'Fix stories';
        nurr.addEventListener('click', doUserStories, false);
        document.body.prepend(nurr);
        
        let stbt = document.createElement('a');
        stbt.id = 'stbt';
        stbt.classList.add('hotlink');
        stbt.innerHTML = '⌛';
        stbt.title = 'STBT Checkout board';
        stbt.href = '/projects/G4ST/board';
        document.body.prepend(stbt);
        
        let board = document.createElement('a');
        board.id = 'board';
        board.classList.add('hotlink');
        board.innerHTML = '💥';
        board.title = 'Current Project';
        board.href = `/secure/RapidBoard.jspa?rapidView=${getCurrentBoard()}&view=planning`;
        document.body.prepend(board);
        
        let sprint = document.createElement('a');
        sprint.id = 'sprint';
        sprint.classList.add('hotlink');
        sprint.innerHTML = '🐎';
        sprint.title = 'Current Sprint';
        sprint.href = `/secure/RapidBoard.jspa?rapidView=${getCurrentBoard()}&sprint=${getCurrentSprint()}`;
        document.body.prepend(sprint);
        
        let sprintSelect = document.createElement('span');
        sprintSelect.id = 'sprintSelect';
        sprintSelect.classList.add('hotlink');
        sprintSelect.innerHTML = '+';
        sprintSelect.title = 'Update Current Sprint';
        sprintSelect.addEventListener('click', selectSprint, false);
        document.body.prepend(sprintSelect);

        let boardSelect = document.createElement('span');
        boardSelect.id = 'boardSelect';
        boardSelect.classList.add('hotlink');
        boardSelect.innerHTML = '+';
        boardSelect.title = 'Update Current Board';
        boardSelect.addEventListener('click', selectBoard, false);
        document.body.prepend(boardSelect);
    }

    updateMenu();
}());

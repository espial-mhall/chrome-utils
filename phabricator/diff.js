const diffPage = '#differential-review-stage';
const diffHeader = '.phui-header-header';
const diffContents = '.phui-property-list-section .phui-property-list-text-content .phab-remarkup';
const diffDetails = '.phui-main-column > .phui-box + .phui-box .phui-property-list-properties';

const sidebar = '.phui-flank-view';
const sidebarHead = '.phui-flank-view-head';
const sidebarHeader = '.phui-flank-header';
const sidebarList =  `${sidebarHead} .phui-list-view`;

const ticketsPattern = /(NEP|AD|MD)-(\d+)(,-?\d+)*/g;
const ticketsParser = /(NEP|AD|MD)+-(.*)/;
const ticketsIdSplit = /[,-]+/;

const jiraPrefix = 'http://jira.mediamiser.internal/browse/';
const diffId = window.location.pathname;

function getJiraTickets(content) {
    const ticketsMatches = content.match(ticketsPattern);
    if (!ticketsMatches) {
        return [];
    }
    const tickets = [];
    
    for (const ticketsMatch of ticketsMatches) {
        const [,project, ids] = ticketsParser.exec(ticketsMatch);
        const idsList = ids.split(ticketsIdSplit);

        idsList.forEach((id) => {
            tickets.push(`${project}-${id}`);
        })
    }

    return tickets;
}
function unique(items) {
    const uniqueItems = {};

    items.forEach(element => {
        uniqueItems[element] = true;
    });

    return Object.keys(uniqueItems);
}
function getAllJiraTickets() {
    const tickets = getJiraTickets(document.querySelector(diffHeader).innerText);
    
    document.querySelectorAll(diffContents).forEach((el) => {
        tickets.push(...getJiraTickets(el.innerText));
    });

    return unique(tickets);
}
function addTicketsToDetails(tickets) {
    const detailsList = document.querySelector(diffDetails);
    const ticketLinks = tickets.map(ticket => `<a href="${jiraPrefix}${ticket}" target="_blank">${ticket}</a>`).join('<br/>');
    detailsList.innerHTML = `
        <dt class="phui-property-list-key">Jira Tickets</dt>
        <dd class="phui-property-list-value">${ticketLinks}</dd>
        ${detailsList.innerHTML}
    `;
}
function addJenkinsToDetails() {
    const detailsList = document.querySelector(diffDetails);
    detailsList.innerHTML = `
        <dt class="phui-property-list-key">Jenkins</dt>
        <dd class="phui-property-list-value">
            <a href="http://neptune-phabricator.agilitypr.internal${diffId}/app/">Hosted</a> | 
            <a href="http://neptune-phabricator.agilitypr.internal${diffId}/app/assets/coverage">Coverage</a>
        </dd>
        ${detailsList.innerHTML}
    `;
}
function addToDetails(tickets) {
    addTicketsToDetails(tickets);
    addJenkinsToDetails();
}
function addSidebarItem(url, text, icon) {
    if (!icon) {
        icon = 'fa-list';
    }
    const list = document.querySelector(sidebarList);
    const item = document.createElement('li');
    item.className = 'phui-list-item-view phui-list-item-type-link phui-list-item-has-icon';
    item.innerHTML = `
        <a href="${url}" target="_blank" class="phui-list-item-href">
            <span class="visual-only phui-icon-view phui-font-fa phui-list-item-icon ${icon}" data-meta="0_2" aria-hidden="true"></span>
            <span class="phui-list-item-name">${text || url}</span>
        </a>
    `;
    list.insertAdjacentElement('afterbegin', item);
}
let giveUp = 5;
function addToSidebar(tickets) {
    const list = document.querySelector(sidebarList);
    if (!list) {
        if (!giveUp--) {
            return;
        }
        requestAnimationFrame(addToSidebar);
        return;
    }

    tickets.forEach((ticket) => {
        addSidebarItem(jiraPrefix + ticket, ticket, 'fa-bookmark');
    });
    // addSidebarItem(`http://neptune-phabricator.agilitypr.internal${diffId}/app/assets/coverage`, 'Coverage', 'fa-check');
    addSidebarItem(`http://neptune-phabricator.agilitypr.internal${diffId}/app/`, 'Hosted', 'fa-chrome');
}


if (document.querySelector(diffPage)) {
    const tickets = getAllJiraTickets();
    addToDetails(tickets);
    addToSidebar(tickets);
}

